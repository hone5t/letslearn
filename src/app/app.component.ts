import { Component } from '@angular/core';

@Component({
  selector: 'lsl-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'LetsLearn';
  points = 1;

  onIncreas() {
    this.points += 1;
  }

}


